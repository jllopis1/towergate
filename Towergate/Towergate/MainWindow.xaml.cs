﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Towergate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<User> items = new List<User>();
            items.Add(new User() { Name = "John Doe", Age = 42 , PostCode = "RTYU", Height = 2000.0});
            items.Add(new User() { Name = "Jane Doe", Age = 39 , PostCode = "TREGU", Height = 4356.0 });
            items.Add(new User() { Name = "Sammy Doe", Age = 13, PostCode = "E1LX", Height = 6745.7 });
            lvDataBinding.ItemsSource = items;
        }


        private void ButtonAddName_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            string age = txtAge.Text;
            string postcode = txtPostCode.Text;
            string height = txtHeight.Text;

            //Regexp
            Regex r = new Regex($"^[a-zA-Z0-9]*$");

            if (!string.IsNullOrWhiteSpace(txtName.Text) && !lvDataBinding.Items.Contains(txtName.Text) && txtName.MaxLength <= 5 
                && Convert.ToInt32(age) >= 0 && Convert.ToInt32(age) <= 110 &&  r.IsMatch(postcode))
            {
                // Create a collection for your types
                ObservableCollection<User> list = new ObservableCollection<User>();
                for (int i = 0; i < lvDataBinding.Items.Count; i++)
                {
                    User user = lvDataBinding.Items.GetItemAt(i) as User;
                    list.Add(new User() { Name = user.Name, Age = user.Age, PostCode = user.PostCode, Height = user.Height });
                };
                list.Add(new User() { Name = name, Age = int.Parse(age), PostCode = postcode, Height = double.Parse(height) });
                lvDataBinding.ItemsSource = list;

            }
        }

        public class User
        {
            public string Name { get; set; }

            public int Age { get; set; }

            public string  PostCode { get; set; }

            public double Height { get; set; }
        }

    }
}
